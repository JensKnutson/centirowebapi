﻿using CentiroWebApi.Models;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace CentiroWebApi.Services
{
    public class JsonOrderService
    {
        public JsonOrderService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }
        public JsonOrderService()
        {
        }

        public IWebHostEnvironment WebHostEnvironment { get; }
        private readonly string path = @"C:\Resources\xml\orders.xml";

        public string getJson()
        {
            List<Order> order = ParseXml();
            string orderJson = ToJson(order);
            return orderJson;
        }

        public List<Order> ParseXml()
        {
            List<Order> order;
            try
            {
                using (Stream reader = new FileStream(path, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<Order>));
                    order = (List<Order>)serializer.Deserialize(reader);
                    return order;
                }
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException(e.StackTrace);
            }
        }

        public string GetOrder(int ordernr)
        {
            List<Order> order = ParseXml();
            order = order.FindAll(x => x.OrderNumber == ordernr);
            string jsonOrder = ToJson(order);
            if (jsonOrder.Equals("[]"))
            {
                return "We could not find an order with ordernumber " + ordernr;
            }
            else
            {
                return jsonOrder;
            }
        }

        private string ToJson(List<Order> order)
        {
            string json = JsonConvert.SerializeObject(order);
            return json;
        }
    }
}
