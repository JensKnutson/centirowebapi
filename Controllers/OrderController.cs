﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentiroWebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace CentiroWebApi.Controllers
{
    [Route("/order")]
    [ApiController]
    public class OrderController : Controller
    {
        public OrderController(JsonOrderService jsonOrderService)
        {
            this.jsonOrderService = jsonOrderService;
        }

        public JsonOrderService jsonOrderService { get; }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("/allorders")]
        public String GetAllOrders()
        {
            return jsonOrderService.getJson();
        }

        [HttpGet]
        public String GetOrder([FromQuery] int ordernr)
        {
            return jsonOrderService.GetOrder(ordernr);
        }
    }
}
